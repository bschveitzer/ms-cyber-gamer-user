export interface IUserModel {
  name: String,
  email: String,
  password: String,
  phone: String,
  gender: String,
  birthdate: Date
}