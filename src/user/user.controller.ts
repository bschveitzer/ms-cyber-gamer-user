import { Controller, Get, Param } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { UserService } from './user.service';
import { User } from 'src/models/user.model';
import { IUserModel } from 'src/interfaces/user.interface';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @MessagePattern('getUser')
  async getUser(data: string): Promise<User[]> {
    return await this.userService.getUser(data);
  }

  @MessagePattern('createUser')
  async createUser(data: User): Promise<User> {
    return await this.userService.createUser(data);
  }

  @MessagePattern('updateUser')
  async updateUser(data: string): Promise<User> {
    return await this.userService.updateUser(data);
  }

  @MessagePattern('deleteUser')
  async deleteUser(data: string): Promise<User> {
    return await this.userService.deleteUser(data);
  }

  @MessagePattern('listUsers')
  async listUsers(): Promise<User[]> {
    return await this.userService.listUsers();
  }
}
