import { Injectable } from '@nestjs/common';
import { User } from '../models/user.model';
import { IUserModel } from '../interfaces/user.interface';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class UserService {
  constructor(@InjectModel(User.name) private userModel: Model<User>) {}

  async getUser(email: string): Promise<User[]> {
    return await this.userModel.find({ email });
  }

  async listUsers(): Promise<User[]> {
    return await this.userModel.find();
  }

  async createUser(data: User): Promise<User> {
    const createdUser = new this.userModel(data);
    return createdUser.save();
  }

  async updateUser(data: any): Promise<User> {
    return await this.userModel.update({ id: data.id }, data.user, { upsert: true });
  }

  async deleteUser(id: string): Promise<User> {
    return await this.userModel.findByIdAndRemove({ id });
  }
}
