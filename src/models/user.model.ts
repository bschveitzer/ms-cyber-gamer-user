import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ timestamps: true })
export class User extends Document {
  @Prop({ required: true, lowercase: true })
  name: string;

  @Prop({ required: true, lowercase: true, unique: true})
  email: string;

  @Prop({ required: true, lowercase: true })
  password: number;

  @Prop({ required: true })
  phone: string;

  @Prop({ required: true, lowercase: true })
  gender: string;

  @Prop({ required: true })
  birthdate: Date;
}

export const UserModel = SchemaFactory.createForClass(User);